#!perl

use strict;
use warnings;
use Test::More tests => 16;
use Perl::Critic;
use Perl::Critic::Config;
use Perl::Critic::Policy::Variables::RequireHungarianNotation;

# import common P::C testing tools
use lib 't/lib';
use PerlCriticTestUtils 'pcritique';

PerlCriticTestUtils::block_perlcriticrc();

my @a_is_tests = (    # [ code, violation count ]
    [ q{ my $i_foo = 12313; },                       0 ],
    [ q{ my $s_bar = 'csacsa'; },                    0 ],
    [ q{ my $bar = (1); },                           1 ],
    [ q{ my $bar = undef; },                         1 ],
    [ q! my $bar = do { qw(mares eat oats eat); } !, 1 ],
    [ q{ local $bar = (1); },                        1 ],
    [ q{ local $ar_bar = []; },                      0 ],
    [ q{ our $bar = (1); },                          1 ],
    [ q{ our $ar_bar = []; },                        0 ],
    [ q{ our $f_bar = 1.123; },                      0 ],
    [ q{ my $f_bar = 1.123; },                       0 ],
    [
        q{
        my $i_bar = shift;
        my $f_bar = 1.123;
        my $a_bar = 1.123;
        my $bar = 1.123;
        my $f_foo = 1.123;
        my $foo = 1.123;
        my $s_foo = '';
        my $fooBar = '';
     }, 4
    ],
    [ q{ my $cr_bar = sub{return 1;} }, 0 ],
    [ q{ my $bar = sub{return 1;} },    1 ],
    [ q{ state $bar = 1; },                        1 ],
    [ q{ state $i_bar = 2; },                      0 ],
);

for (@a_is_tests) {
    my ( $s_perl, $nviol ) = @{$_};
    my $s_policy = 'Variables::RequireHungarianNotation';
    is( pcritique( $s_policy, \$s_perl ), $nviol, $s_perl );
}
