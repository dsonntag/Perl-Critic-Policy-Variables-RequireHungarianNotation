#!perl

use strict;
use warnings;
use Test::More tests => 6;
use Perl::Critic;
use Perl::Critic::Config;
use Perl::Critic::Policy::Variables::RequireHungarianNotation;

# import common P::C testing tools
use lib 't/lib';
use PerlCriticTestUtils 'pcritique';

PerlCriticTestUtils::block_perlcriticrc();

my @a_is_tests = (    # [ code, violation count ]
    [ q{ my @a_foo = (); },                      0 ],
    [ q{ my @a_bar = (); },                      0 ],
    [ q{ my @bar = (1); },                       1 ],
    [ q{ my @bar = qw(mares eat oats); },        1 ],
    [ q! my @bar = do { qw(mares eat oats); } !, 1 ],
    [ q{ (my @a_foo = ()) },                     0 ],
);

for (@a_is_tests) {
    my ($s_perl, $i_nviol) = @{$_};
    my $s_policy = 'Variables::RequireHungarianNotation';
    is(pcritique($s_policy, \$s_perl),
        $i_nviol, sprintf("%2d violation%s in %s", $i_nviol, $i_nviol == 1 ? ' ' : 's', $s_perl));
}
