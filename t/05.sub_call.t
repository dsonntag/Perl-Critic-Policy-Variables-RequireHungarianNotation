#!perl

use strict;
use warnings;
use Test::More tests => 3;
use Perl::Critic;
use Perl::Critic::Config;
use Perl::Critic::Policy::Variables::RequireHungarianNotation;

# import common P::C testing tools
use lib 't/lib';
use PerlCriticTestUtils 'pcritique';

PerlCriticTestUtils::block_perlcriticrc();

my @a_is_tests = (    # [ code, violation count ]
    [
        q/
    sub test{ my ($self, $inv_foo)= @_; return 1 }
    sub test_2{ my ($self, $obj_foo)= @_; return 1 }
    /, 0, 'custom call test run'
    ],
    [
        q/
    sub test{ my ($self, $i_foo)= @_; return 1 }
    sub test_2{ my ($self, $s_foo)= @_; return 1; }
    sub test_3{ my ($self, $s_foo, $ar_foo)= @_; return 1 }
    /, 0, 'scalar call test run'
    ],
    [
        q/
    sub test{ my ($self, $foo)= @_; return 1 }
    sub test_2{ my ($self, $s_foo)= @_; return 1; }
    sub test_3{ my ($self, $s_foo, $foo)= @_; return 1 }
    /, 2, 'scalar call test fails'
    ],
);

for (@a_is_tests) {
    my ( $s_perl, $nviol, $s_name ) = @{$_};
    my $s_policy = 'Variables::RequireHungarianNotation';
    is( pcritique( $s_policy, \$s_perl, { custom => 'inv obj' } ), $nviol, $s_name );
}
