#!perl

use strict;
use warnings;
use Test::More tests => 5;
use Perl::Critic;
use Perl::Critic::Config;
use Perl::Critic::Policy::Variables::RequireHungarianNotation;

# import common P::C testing tools
use lib 't/lib';
use PerlCriticTestUtils 'pcritique';

PerlCriticTestUtils::block_perlcriticrc();

my @a_is_tests = (    # [ code, violation count ]
    [ q{ my $inv_foo = 12313; },                     0 ],
    [ q{ my $obj_bar = 'csacsa'; },                  0 ],
    [ q{ my $bar = (1); },                           1 ],
    [ q{ my $bar = undef; },                         1 ],
    [ q! my $bar = do { qw(mares eat oats eat); } !, 1 ],
);

for (@a_is_tests) {
    my ( $s_perl, $nviol ) = @{$_};
    my $s_policy = 'Variables::RequireHungarianNotation';
    is( pcritique( $s_policy, \$s_perl, { custom => 'inv obj' } ), $nviol, $s_perl );
}
